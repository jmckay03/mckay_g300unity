﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSpawning : MonoBehaviour {

	public Transform healthDrop;
	public int numToSpawn = 1;
	public Vector3 position;
	public float spawnTime = 60f;


	void Start() {

		InvokeRepeating ("Spawn", spawnTime, spawnTime);


	}


	void Spawn () {

		int spawned = 0;

		while (spawned < numToSpawn) {

			Vector3 position = new Vector3(Random.Range(-15.0F, 20.0F), 0.5f, Random.Range(-15.0F, 20.0F));

			spawned++;
			Instantiate (healthDrop, position, Quaternion.identity);
		}

	}
}

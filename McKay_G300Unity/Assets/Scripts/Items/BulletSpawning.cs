﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawning : MonoBehaviour {


	public Transform ammoDrop;
	public int numToSpawn;
	public Vector3 position;
	public float spawnTime = 30f;


	void Start() {

		InvokeRepeating ("Spawn", spawnTime, spawnTime);


	}
	

	void Spawn () {

		int spawned = 0;

		while (spawned < numToSpawn) {

			Vector3 position = new Vector3(Random.Range(-15.0F, 20.0F), 0.5f, Random.Range(-15.0F, 20.0F));

			spawned++;
			Instantiate (ammoDrop, position, Quaternion.identity);
		}
		
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthAdder : MonoBehaviour {
	
	Transform player;
	public int health_adder = 20;
	PlayerHealth playerHealth;
	public Slider healthSlider;


	void OnTriggerEnter (Collider other) {
		
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		playerHealth = player.GetComponent <PlayerHealth> ();
		playerHealth.currentHealth = playerHealth.currentHealth + health_adder;

		playerHealth.healthSlider.value = playerHealth.healthSlider.value + health_adder;


		Destroy (gameObject);

	}
}

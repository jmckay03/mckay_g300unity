﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletAdder : MonoBehaviour {

	public int bullet_count;


	void OnTriggerEnter (Collider other) {

		PlayerShooting.bullets += bullet_count;
		BulletManager.bullet += bullet_count;
		Destroy (gameObject);

	}
	

}
